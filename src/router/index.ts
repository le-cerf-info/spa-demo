// src/router/index.ts
import { createRouter, createWebHistory } from 'vue-router';

const routes = [
    {
        path: '/',
        name: 'feedback',
        component: () => import('@/views/Home.vue')
    },
    {
        path: '/thank-you',
        name: 'thanks',
        component: () => import('@/views/ThankYou.vue')
    }
];

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
});

export default router;
