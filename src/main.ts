// src/main.ts
import { createApp } from 'vue';
import { pinia } from './stores';
import router from './router';
import App from './App.vue';

createApp(App).use(pinia).use(router).mount('#lci-demo');
